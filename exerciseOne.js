function ceroMatrix(matrix) {

    //arrays for save the columns numbers that has 0
    var colCero = [];
    var rowCero = [];

    matrix.forEach( function (row,indexRow) {
        //check if there is a 0 in the row
        if (matrix[indexRow].indexOf(0) > -1){
            //save the number of row where there is a 0
            rowCero.push(indexRow);
            //iterate over the row to save the number of columns thas has 0
            row.forEach(function (col,indexCol) {
                //save the columns numbers
                if (matrix[indexRow][indexCol] === 0) colCero.push(indexCol);
            });
        }
    });

    //Iterate one more time over the matrix to update it
    matrix.forEach(function (row,indexRow) {
       row.forEach(function (col,indexCol) {
           //update the values according to the number of columns or rows obtained previously
           if ( colCero.indexOf(indexCol) > -1 || rowCero.indexOf(indexRow)> -1) matrix[indexRow][indexCol] = 0;
       })
    });

    return matrix;
}

console.log(ceroMatrix([[5,5,1,9], [0,2,9,0], [7,9,8,9]]));