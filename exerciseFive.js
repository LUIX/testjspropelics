function romanize(decimalNumb) {
    var romanString = '';
    //we need and object to map the decimal value of the letter whit the alphabet letters;
    var charactersValues = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1};
    //itarate on the roman values to calculate the number
    for ( var i in charactersValues ) {
        //decresing the value of the decimal number and build the roman number sring
        while (decimalNumb >= charactersValues[i]){
            //concatenate the key value of the charactersValues
            romanString += i;
            //decrease the decimal number whit the value of the object charactersValues
            decimalNumb -= charactersValues[i];
        }
    }
    return romanString;
}

console.log(romanize(512));