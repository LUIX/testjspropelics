function vowelCount(stringData) {
    //array to compare the value of the vowels
    var vowels = ['a','e','i','o','u'];
    //counter of vowels
    var totalVowels = 0;
    //iterate over the string to count the vowels
    for (var i in stringData){
        //check if the caracter is a vowel
        if (vowels.indexOf(stringData[i]) > -1){
            //increment the counter of vowels
            totalVowels++;
        }
    }
    return totalVowels;
}

console.log(vowelCount('parangruudffo'));