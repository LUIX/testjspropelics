function transposeMatrix(matrix) {
    //number of columns
    var colsLength = matrix[0].length;
    //array to create the new matrix
    var colArray = [];

    //the new array needs an empty arrays to fill later whit the new dimensions
    //Transpose matrix -> Matrix[Columns]*[Rows]
    for (var cols=0;cols<colsLength;cols++){
        colArray.push([]);
    }

    //iterate over the matrix
    matrix.forEach(function (row,indexRow) {
        row.forEach( function (col,indexCol) {
            //fill the new array whit values using preview array structure
            colArray[indexCol].push(matrix[indexRow][indexCol]);
        });
    });

    return colArray;
}

console.log(transposeMatrix([[1,2,3],[4,5,6]]));
